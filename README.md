# Ansible Collection - s3lph.irc

Work in progress.  The following can be done with this collection so far:

- Deploy InspIRCd and Anope
- Configure the most important InspIRCd config types (server, network, oper, opertype, classes, ssl_gnutls, bind, link, uline, files/motd)
- Configure all global Anope options
- Configure the most important NickServ options
- Configure the most important ChanServ options

The following is not (yet) supported:

- Configuration all InspIRCd options (performance, ...)
- Configuration any Anope modules apart from NickServ and ChanServ
- Customization of commands and optional modules for Anope modules

Documentation will follow some time in the future...